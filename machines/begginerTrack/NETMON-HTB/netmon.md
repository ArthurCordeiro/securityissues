#ABSTRACT
FTP enum  + Windows + PowerShell + File Misconfiguration 

techniques: nmap bruteforce, some manipulation with smb, references for inline reverse 
shells


Such a Great Machine! Here, we can learn a little bit about windows systems and running applications. Somethings about injections in a misconfigurated application

#USER
We did some scans with nmap and this machine has so much services running and we staerted
from the first (FTP) and we got lucky to get the user flag with ftp.
https://www.msnoob.com/ftp-commands-for-windows.html

After that, we thing to attack the smb service, with the guest user. Trying to brutefroce
the smb service we found that the guest user is disabled and got the "" password.

Reference for bruteforce with nmap https://nmap.org/nsedoc/scripts/smb-brute.html
              smbclient commands https://www.computerhope.com/unix/smbclien.htm

#Priv Escalation - ROOT
We didn't get so much things from smb service, and back to http running on 80. We found
some Network Monitor (PRTG) running and it had some exploits avaiable

https://www.exploit-db.com/exploits/49156

PRTG Network Monitor 18.2.38 - (Authenticated) Remote Code Execution      | exploits/windows/webapps/46527.sh
PRTG Network Monitor < 18.1.39.1648 - Stack Overflow (Denial of Service)  | exploits/windows_x86/dos/44500.py
PRTG Traffic Grapher 6.2.1 - 'url' Cross-Site Scripting                   | exploits/java/webapps/34108.txt

We've been almost there but, to get the RCE working we need somehow to get some acc with
manager credentials. To do it, we need to Google How PRTG Monitor Stores in Data to get
some logs wich PRTG save it and there's the acc credentials that we need. From now and on,we've following such a great David's tuto:

https://davidhamann.de/2020/01/22/htb-writeup-netmon/

#References
https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data
https://davidhamann.de/2020/01/22/htb-writeup-netmon/
https://www.codewatch.org/blog/?p=453
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#powershell
