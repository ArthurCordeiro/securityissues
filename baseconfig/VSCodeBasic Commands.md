# VSCodeBasic Commands:
***

> To add some customized hotkey 
> Under Linux this file should be in ~/.config/Code/User/keybindings.json


1. To navifate in the file tree with keyboards
    .CTRL + E
2. To split pane horizontally
    .CTRL + \
3. To open integrated terminal
    .CTRL + `
4. To switch between panes
    .CTRL hjkl
5. To switch between buffers 
    .ALT + (1, 2, 3, 4 ... )
6. To open some file
    .CTRL + o
7. To search some string
    .CTRL + F
~~ 8. Initial keybinding for Go To Position ~~
    ~~.⌃+Alt+G ~~
8. To open the result of the file 
    .CTRL + V
9. (Customized) To turn on/off vim mode
    .CTRL + 0
> Need to install vim extension



**Some Interesting Reference:**
---
[Switch Panes](https://www.ryanchapin.com/vscode-keyboard-shortcut-to-navigate-between-split-panes/)