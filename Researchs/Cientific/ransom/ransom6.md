# Malware Detection Using Honeypot and Machine Learning

## Intro Considerations

The advantage of the Decision Tree Tree algorithm is that it has high accuracy in detection malware, easy to generate rules, easy to understand and also
reduce the complexity. The Support Vector Machine algorithm has the advantage of detecting malware quickly.

## Dataset
The Endgame Malware Benchmark for Research (EMBER) dataset is used. This dataset consists of 900,000 training samples (300,000 malicious samples,
300,000 benign samples, and 300,000 samples unabeled).




#Notes:

