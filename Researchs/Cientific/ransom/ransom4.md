#Title: Ransomware Evolution, Growth and Recommendation for Detection

Up to this moment (2020), the main methods used by attacker to infect your machine are malicious emails and malicious links.

## Encryption Methods Used in Ransomware

Ransomware Attackers mainly used symmetric encryption methods such as RC4 (Rivest Cipher 4), AES (Advanced Encryption Standart). But nowadays attackers
prefer hybrid methods of encryption. Today's ransomware attackers send only public key to the infected machine, and the private key in stored in the C&C
servers. Symmetric key is used in the encryption in the first stage, then encryption is done in the second stage with RSA (Rivest-Shamir-Adleman) public
key.


#Notes:

File signature may be a good way to detect ransom?
(Technique used for detect ransom)

Technique used for signature-detection: Dynamic and Static Heuristic Detection Code for white/black, analyses the frequencies of registered signatures
and unregistered signatures and checks that any codes and drivers are incvludede in unregistered signatures to analyze and situational frequency.

