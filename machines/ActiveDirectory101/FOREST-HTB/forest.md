#ABSTRACT
smbEnum + Metasploit + windows

techniques: nmap vulns scan, some manipulation with smb


Such a Great Machine! Here, we can learn a little bit about windows systems and running applications. Somethings about reversing shell and msfconsole exploits.

#USER
After some scan with nmap

$ nmap -A -p 1-1000 -v 10.10.10.40 | tee nmap_initial.log

We found open services running on this machine. After that we found some smb service
smbV1 running on a windows 7 OS. 

So we used some nmap vulns scan and we got it.

$ nmap --script smb-vuln* -p 445,135 10.10.10.40

and we get the vulns the gave us nt authority system = root privs

for it we used msfconsole.

#priv Escalation - ROOT


#References
https://www.hackingarticles.in/password-crackingsmb/
