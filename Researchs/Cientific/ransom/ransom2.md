#Title: Detecting crypto-ransomware in IoT networks based on energy consumption footprint

In this paper they present a machine learning based approach to detect ransomware attacks by monitoring power of consumption of Android devices.

To record this power of consumption they used PowerTutor to monitor and sample power usagfe of all running processes in 500ms intervals.

They used PowerTutor to monitor and record the device processe's power usage for 5 min. This procedure was repeated five times per device; thus they
obtained 15 power usage samples for each and every application and ransom.

They applied four state-of-the-art classifiers: K-Nearest Neighbor (KNN), Neural Network (NN), Support Vector Machine (SVM) and Random Forest (RF), on the power usage samples to recognise the class of each sequence of power consumption.

Power usage sequence of each process can be considered as time-series data. In this study, a distance based time-series classication approach based on Dynamic Time Warping (DTW).

Metrics and cross-vallidation:

True Positive : Indicates that a ransomware is corectly predicted as a malicious application.
True Negative : Indicates tha that a goodware is detected as a on-malicious application correctly.
False Positive : Indicates that a goodware is mitakenly detected as a malicious application .
False Negative : Indicates thta a ransomware is not detected and labelled as a non-malicious application.

Machine leearning objective performance evaluation metrics that are commonly used in literature, namely: Accuracy, Recall, Precision and F-Measure.



#Notes:
File signature may be a good way to detect ransom?

All ransomware were downloaded via VirusTotal Intelligence API, and these ransomware have active Command and Control (C2) servers.


