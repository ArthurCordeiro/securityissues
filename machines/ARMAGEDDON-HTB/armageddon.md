This is a write up for HTB machine.
#USER

## Nmap for scan:

> $ nmap -sC -sV -O -script vulns -v -p - <ip_adress> | tee nmap.log


## Found some hidden files.
/robots.txt
/CHANGELOG.txt // Have some info about Drupal CMS. 


## Exploit Link
https://github.com/dreadlocked/Drupalgeddon2
https://www.yeahhub.com/drupal-7-exploitation-metasploit-framework-sql-injection/

Got a meterpreter shell!

##Enumerating Inside Server
Use grep command to find some key strings in files is a good practice.

Gave us an user name drupaluser
##grep -r password

 'username' => 'drupaluser',                                                                                  
 'password' => 'CQHEy@9M*m23gBVj'

 Is a mysql user.

 (Mysql Structure) ==> (DATABASE) ==> (TABLES) ==> (TABLE'S CONTENTS);

## sql query to get brutherealadmin's pass hashed
mysql -u drupaluser -pCQHEy@9M*m23gBVj -D drupal -e 'select name,pass from users;'

the user's password: booboo

#ROOT

https://www.piratemoo.com/armageddon/
https://jeevanachandra.github.io/hackthebox/2021/03/29/Armageddon.html

