USER

## NMAP for scan 

> $ nmap -sV -sC --script vulners -v -p- 10.10.10.242 | tee nmap.log

## Try to Bruteforce WebDomain
 
 Domain bruteforce with wfuzz

> $ wfuzz -c -v -z file,big.txt --hc '404' http://10.10.11.100/FUZZ
> add some .php extensions to bruteforce



## Found user and pass XXE injection

user: development
pass: m19RoAU0hP41A1sTsq6K


## Enumerating WebServer
In portal, found some script and there some XML version with vulns. XXE attack que executed using burpsuit.

Likewise, I got the user flag. I copied my public key to the machine to get the SSH access. Somehow, the private key of the target didn’t work for me.

#Priv Escalation - ROOT


https://www.0xsakthi.rocks/2021/07/bounty-hunter-hackthebox-writeup.html
