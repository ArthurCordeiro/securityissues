#!/bin/bash

LISTA_PROGRAMAS_APT=(
	git
	vim
	curl
	wget
	ninja-build
	gettext
	libtool
	libtool-bin
	autoconf
	automake
	cmake
	g++
	pkg-config
	unzip
	doxygen
	pip
	cargo
)

instalar_pacotes_apt () {
	for pacote in ${LISTA_PROGRAMAS_APT[@]}; do
		sudo pacman -S $pacote -y
	done
}

instalar_pacotes_apt

# se nao funcionar, roda na mao mesmo
curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

echo "To finish, close this terminal, open a new one and type 'nvm install v17.8.0'"
