#Continuation...

Topic: Local File Inclusion 
***

In most cases, we have to do a black box pentesting in a web applicantion. So, in this case
try to get errors report can be a nice method to get some hints. 

We have some technique that we can use, and some of them we can show up quickly on this tuto.

1.NUll byte is an injection technique where URL-encoded representation such as %00 or 0x00 in
hex with user supplied data to terminate strings.

Ex : `include("languages/../../../../../etc/passwd%00").".php");`

2. In php are two methods that can be used to bypass the filter. First the, NullByte or the 
current directory trick at the end of the filtered keyword /.

The exploit will be similar to `http://webapp.thm/index.php?lang=/etc/passwd/.`

3. Some input validation filters are avaiable but not perfect at all. One of these validations
is that web app replaces the **../** with the empty string.

We can send the following payload to bypass it: `....//....//....//....//....//etc/passwd`

4. In some cases the developer forces the include to read from a defined directroy and we have
to add this file in the payload.


Topic: Remote File Execution
___

![RFI Diagram](https://drive.google.com/file/d/1eXSUmdDaJuMmWuhIUZM8Meviikj6S9O5/view?usp=sharing)



Steps For Testing LFI
___



1.Find an entry point that could be via GET, POST, COOKIE, or HTTP header values!
2.Enter a valid input to see how the web server behaves.
3.Enter invalid inputs, including special characters and common file names.
4.Don't always trust what you supply in input forms is what you intended! Use either a browser
address bar or a tool such as Burpsuite.
5.Look for errors while entering invalid input to disclose the current path of the web application; if there are no errors, then trial and error might be your best option.
6.Understand the input validation and if there are any filters!
7.Try the inject a valid entry to read sensitive files



Challengers 
---
It might be a goode idead to conclude these exercices with burp!

The last challeng RCE + RFI 

<?php print exec('hostname'); ?>





Topic SSRF - Server-Side Request Forgery

***

It's a vulnerability that allows a malicious user to cause the webserver to make an additional
or edited HTTP request to the resource of the attacker's choosing.


SSRF TRICKS AND EXAMPLES
___

look at the following URL:

`https://website.thm/item/2?server=server.website.thm/flag?id=9&x`

In this url, an URL attack is been occurred. As parameter we use the following API request

`server.website.thm/flag?id=9`

The link without any malicious content:

`https://website.thm/item/2?server=api`

Take note of the payload ending in &x= being used to stop the remaining path from being ap
pended to the end of the attacker's URL and instead turns it into a parameter (?x=) on the que
ry string.
i

Finding an SSRF
___

If the victm do not show up to us how the api is being requested some external HTTP tool 
might be a good idea. In this case,  ** requestbin.com ** generate some default request models
to explore some apps.


Defeating Common SSRF Defenses
___

Some techniques trying to stop some SSRF attack can be used when security-savvy developers im
plement it.

Deny List
Accept all requests except for those wich are specified on the list. An Attacker can bypass it
using some alternatives localhost references such as 0, 0.0.0.0, 0000, 127.1, 127.*.*.*, 
2130706433, 017700000001 or subdomains that have a DNS record which resolves to the IP Address
127.0.0.1 such as 127.0.0.1.nip.io.

Allow List
All request get denied, unless they appear on a list or match a particular pattern. An at
tacker could quickly circumvent this rule by creating a subdomain on an attacker's domain na
me, such as https://website.thm.attackers-domain.thm. The application logic would now allow
this input and let an attacker control the internal HTTP request.



Cross-site Scripting


