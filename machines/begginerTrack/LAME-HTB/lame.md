#ABSTRACT
Lame is a Linux machine with some network protection wich prevent an attacker to free surf blocking some assets discovery

We've learn a little about attacking FTP service and discovered some techni
que named "Banner Grabbing".

We've used some of GTFOBins references for our priv escal when we were in. 

An alternative writeup by https://software-testing.com/topic/104305/what-does-pn-option-mean-in-nmap teach a little about
smb enum and priv escalation with some smb exploit.

#USER 

## NMAP for scan 

> $ nmap -sV -sC -Pn -A -v -p- 10.10.11.103 | tee nmap.log

Here a reference for what means -Pn flag on nmap command: https://software-testing.com/topic/104305/what-does-pn-option-mean-in-nmap

Then we found some services like, FTP, SSH and NetBios, wich means that we had some SMB service on it. But when we did all the service port scan with
nmap we found some suspicious service.

3632/tcp open  distccd

We weren't familiar with this service so : https://en.wikipedia.org/wiki/Distcc

And we found some exploit for an specific version of it. https://github.com/galenlim/distcc-exploit-python/blob/master/README.md
After some adjusts in the exploit source code, we were in as deamon.

A nice try after we got the shell if, for some reason, we do not have acess to some shell spawn, wold be nice
to try rise some shell with python (in linux env): 

$ python -c 'import pty; pty.spawn("/bin/bash")'

 




#Priv Escalation - ROOT
To get wich applications are installed 

ls -alh /usr/bin/ 

get from https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/
and we saw that nmap is avaible for daemon user and using GTFOBins references with nmap interative session we get the root

$ nmap --interative
nmap > !sh

and we got the root!

#References

https://pentestlab.blog/2012/03/01/attacking-the-ftp-service/
https://github.com/galenlim/distcc-exploit-python/blob/master/README.md
https://www.infosecademy.com/netcat-reverse-shells/
https://software-testing.com/topic/104305/what-does-pn-option-mean-in-nmap
