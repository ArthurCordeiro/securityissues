#Title: A Multi-Classifier Network-Based Crypto Ransomware Detection System: A case Study of Locky Ransomware

Mordern host-based detection methods require the host to be infected first in oderder to identify anomalies and detect malware. By the time of infection
it can be too late. Conversely, the network-based methods can be effetive in detecting ransomware attacks, as most ransomware families try to connect to
command and control servers before their harmful payloads are executed.

Crypto ransomware is not only capable of encrypting users's files, it attempts to encrypt anu file located on both mapped and unmapped networks drives.
Crypto ransomware is divided into three types, based on the employed cryptosystem:

Symemetrical Cryptosystem Ransomware: Employs a simmetrical encryption algorithm, DES or AES ti encrypt the victim's files, using the same key for 
encryption and decryption.

Asymmetrical Cryptosystem Ransomware: In this type, a public key, embedded within the ransomware file or downloaded during the communication with the
command and control (C&C) server, is used to encrypt the victim's files.

Hybrid Cryptosystem Ransomware: This uses a  dynamically generated symmetric key to encrypt the victim's files and pre-loaded public key to encrypt the 
symetric key itself, after clearing it out of memory.

Locky (Ransom.Locky) is a hybrid cryptosystem ransomware family, which was released in February 2016 and was considered to be one of the major ransomwar
threats in 2017 by Symantec [5].It is mainly distributed by spam campaigns however, it can be distributed using exploit kits, e.g., Neutrino and Rig.Af
ter infection, it scans the victim’s drives, including the network drives, for specific file types, e.g.,.pdf,.doc,.jpg, etc. to encrypt them using AES and RSA algorithms [14]. One of Locky’s key features is the use of Domain Generation Algorithm (DGA) [15] to find its C&C server. While being one ofthe 
most recent and prevalent ransomware families during 2017 [7], [8], Locky also relies heavily on external communications, making it ideal for a use-case
study. (REFERENCES)

For reference to investigate:

Cabaj et al. [16] analyzed CryptoWall ransomware dynamically in a dedicated environment using a honeypot and automatic run-time malware analytical system, Maltester.

Ahmadian et al. [12] proposed the Connection Monitor & Connection Breaker (CM&CB) framework to detect ‘high survivable ransomwares’ which employ Domain Generation Algorithm (DGA). The proposed method analyzes the requested domain names against a previously trained English language Markov-chain model.

Jones and Shashidhar [18] analyzed the WannaCry ransomware in a Win32 environment using static and dynamic host-based analysis techniques. 

Cabaj et al. [19] presented a dedicated Software-Defined Networking (SDN) based approach to detect and mitigate a ransomware attack, focusing on the analysis of HTTP traffic.

Greg Cusack et al. [20] used a recent property of networking hardware called programmable forwarding engines (PFEs) to monitor the network traffic between an infected computer and the C&C server.



## Settting Up the Analysis Environment
This works section describes the preparation of an analysis environment to run and analyze the network of Locky Ransomware.

A. Malware Testbed Environment

Dynamic analysis of ransomware requires a safe environment to execute and monitor the ransomware’s network activities. We have built a dedicated testbed that consists of three real computers and two virtual machines as depicted in Fig. 2. The purpose of this testbed is to execute samples of ransomware and capture their network traffic. These PCAP files are then analyzed to extract a set of network features that describe the communication behavior between the ransomware and its C&C server (i.e., attacker). The details of the testbed’s components are summarized below:

    PC1 is used as a victim machine, where the ransomware is injected.

    PC2 and its hosted virtual machines VPC1 and VPC2, are three clean machines on the network representing the non-infected nodes. Analyzing their network traffic allows investigation of how ransomware tries to propagate over a network.

    PC3 captures the network traffic and stores it in PCAP files using Wireshark for further analysis. For that, its NIC is set to work in promiscuous mode. As it is important to keep PC3 protected from the ransomware’s propagation attempts, Ubuntu Linux was used as the OS, as the selected ransomware samples do not infect Linux systems. Furthermore, we have setup a firewall with a strict rule to drop any packet targeting PC3 explicitly.

    This testbed is isolated from the university’s main campus network to protect the main network from the ransomware propagation attempts. All of the testbed machines are connected to the Internet, as often required by the ransomware to communicate with its C&C server. The external ISP providing the dedicated Internet connection to the experimental-lab was informed in advance about an infected PC with ransomware on their public network.


B. Ransomware Dataset

Having a rich dataset is crucial in allowing accurate analysis and the extraction of informative network features. The acute shortage in ransomware datasets [21] was a key challenge in this research work. We have examined several benchmark datasets such as [27], [28]:

    KDDCUP 99

    NSL-KDD (2009)

    UNSW-NB15 (2015)

    Canadian Institute for Cybersecurity (CIC) datasets

Unfortunately, these datasets do not include records of any ransomware attack, although the first attack was discovered in 1989 [29]. Therefore, we built our own dedicated testbed to create a new dataset taking the Locky ransomware as a case study as outlined above.

However, we have observed that our testbed-generated dataset only contains unidirectional traffic, i.e., the traffic originated from Locky (at the victim’s end) attempting to find the C&C server, without any response traffic originating from the attackers’ side. This can be due to the likelihood of the C&C servers being down after the campaign was over, their IP addresses were blacklisted, or the attacker being able to recognize our environment as a testbed, rather than a real victim.

Therefore, we have carried out an extensive search to find a reliable dataset containing bidirectional traffic captured when the campaign was still active. We have found a dataset created within a research center in the Czech Technical University (CTU), the Malware Capture Facility Project (MCFP) dataset [30]. In this project, real malware was executed within a real network environment for up to several weeks, around the first appearance of Locky. It also contains a significant amount of benign traffic, the size of which is much larger than the malicious traffic, as it contains all traffic generated by the different benign services running on the system. Conversely, the malicious dataset contains the traffic generated only by Locky, which deliberately tries to send a small amount of traffic over time in order to avoid detection by IDS systems. Table 1 shows the details of the MCFP portion used in this research.


C. Locky Ransomware Sample Collection

Locky samples have been collected from the following open source communities:

    http://virusshare.com

    http://malware-traffic-analysis.net

The hash values and types of the samples were checked using http://virustotal.com to ensure that the samples are valid and have the correct labels.


## Further Featurs of DNS
A total of 9 DNS raw features were extracted and fed into a feature selection engine using Weka, a popular machine learning tool.


## Models Building
Several popular classification learning algorithms such as Random Forest, LibSVM, Bayes Net, and Random Tree were selected to build each classifier C1 and C2 autonomously. Finally, the algorithm that provides the highest accuracy was adopted by each classifier accordingly.

#Notes:

In general, exists two types to treat a ransomware detection methods, host-based methods, in short consist to detect some malware analysing it's structu
re like, code signature, etc... and we have the network-based methods wich can be usefull when some infected file try to connect to commnad and 
control servers and before their hardmful payloads are executed.
