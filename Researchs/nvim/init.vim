call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'terryma/vim multiple cursors'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'roxma/nvim-completion-manager'
Plug 'jiangmiao/auto-pairs'
Plug 'w0rp/ale'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
call plug#end()


colorscheme gruvbox
set background=dark
set hidden
set number
set relativenumber
set mouse=a
set inccommand=split


let mapleader="<\space>"
nnoremap <leader>; A;<esc>
nnoremap <c-p> :Files<cr>
nnoremap <c-f> :Ag<space>
nnoremap <c-s> :w<cr>
nnoremap <c-w> :q!<cr>
nnoremap <c-q> :wq!<cr>
nnoremap <c-o> :e<space>
nnoremap <c-b> :buffer<cr>
nnoremap <c-r> :%s///g
nnoremap <c-t> :terminal<cr>nvim<cr>
let g:UltiSnipsEditSplit = 'vertical'
let g:UltiSnipsSnippetsDir = '~/.config/nvim/UltiSnips'


