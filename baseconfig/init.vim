"good reference for build your own vim.rc
"https://jdhao.github.io/2018/12/24/centos_nvim_install_use_guide_en/


set nocompatible

call plug#begin()

" Color Scheme Install 
Plug 'arcticicestudio/nord-vim'


" Activate Nord Vim when toggling the NERDTree.
Plug 'arcticicestudio/nord-vim', { 'on':  'NERDTreeToggle' }


" plug for Nerd Tree and icons
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'

"Plug for fuzzy finder 
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"Plug 'junegunn/fzf.vim'

" A collection of language packs for Vim
Plug 'sheerun/vim-polyglot'


"Plug for semmantic highlighting in python for vim
"Plug 'numirias/semshi', { 'do': ':UpdateRemotePlugins' }

"Vim plug for Python
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }

"Plug for search specific content inside files
Plug 'dyng/ctrlsf.vim'

"Plug for Linting and Fixing
Plug 'dense-analysis/ale'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'


"Plug for autocomplete in nvim
Plug 'nvim-lua/completion-nvim'

"status bar for nvim
Plug 'vim-airline/vim-airline'


"To comment out a single line, use <leader>cc (leader is a prefix key in Nvim, 
"the default leader key is backslash \); to uncomment a line, use <leader>cu.
"plug for comment a line
Plug 'scrooloose/nerdcommenter'

"automatic quote and bracket completion
Plug 'jiangmiao/auto-pairs'

"Theme for nvim
Plug 'morhetz/gruvbox'

"Plug for highlight my yanked area
Plug 'machakann/vim-highlightedyank'

"Plug for syntax check
Plug 'neomake/neomake'


"Code jump PLugin
Plug 'davidhalter/jedi-vim'
call plug#end()

"config for neomake
"to set pylint as our server checker
let g:neomake_python_enabled_makers = ['pylint']"Using deoplete


"We can also enable automatic code check
call neomake#configure#automake('nrwi', 500)




"config \ neomake







"window will not disappear after auto-completion is done. According to discussions 
"here, we can use the following setting to close the preview window automatically:
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif


"navigate through the auto-completion list with Tab
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

"preview windows will open on top of current window. You can change this behaviour 
"by adding set splitbelow in the configuration file, to use deoplete
set splitbelow

"deoplete \ config

" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0
"open go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"

"Setting coloscheme and background
colorscheme gruvbox
set background=dark

let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard'] "Hide files in .gitignore
let g:ctrlp_show_hidden = 1   

set nocompatible            " disable compatibility to old-time vi
set showmatch               " show matching brackets.
set ignorecase              " case insensitive matching
set mouse=v                 " middle-click paste with mouse
set hlsearch                " highlight search results
set autoindent              " indent a new line the same amount as the line just typed
set number                  " add line numbers
set wildmode=longest,list   " get bash-like tab completions
set cc=80                   " set an 80 column border for good coding style
filetype plugin indent on   " allows auto-indenting depending on file type
set tabstop=4               " number of columns occupied by a tab character
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing

"Config for NerdTree
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeIgnore = []
let g:NERDTreeStatusline = ''
" Automaticaly close nvim if NERDTree is only thing left open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Toggle
nnoremap <silent> <C-t> :NERDTreeToggle<CR>


" open new split panes to right and below
set splitright
set splitbelow
" turn terminal to normal mode with escape
tnoremap <Esc> <C-\><C-n>
" start terminal in insert mode
au BufEnter * if &buftype == 'terminal' | :startinsert | endif
" open terminal on ctrl+n
function! OpenTerminal()
  split term://bash
  resize 10
endfunction
nnoremap <c-n> :call OpenTerminal()<CR>



" use alt+hjkl to move between split/vsplit panels
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

"hotkey for fuzzy finder  
nnoremap <C-o> :FZF<CR>
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-s': 'split',
  \ 'ctrl-v': 'vsplit'
  \}

" Using Lua functions
"nnoremap <C-p> <cmd>lua require('telescope.builtin').find_files()<cr>
"nnoremap <C-g> <cmd>lua require('telescope.builtin').live_grep()<cr>
"nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
"nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>






" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif


" Autoindent Vim Polyglot tries to automatically detect indentation 
"let g:polyglot_disabled = ['autoindent']

"Hot key for find specific content inside a file
nmap     <C-F>f <Plug>CtrlSFPrompt                  
nmap     <C-F>n <Plug>CtrlSFCwordPath
nmap     <C-F>p <Plug>CtrlSFPwordPath


"hot key for /n a line
nmap OO O<Esc>j

"#explicitly specify which ones you're going to use with a particular filetype
"#with ale
"let g:ale_linters = {
"      \   'python': ['flake8', 'pylint'],
"      \   'ruby': ['standardrb', 'rubocop'],
"      \   'javascript': ['eslint'],
"      \}
"

"Pressing F10 or saving the file it fix all code problems use fome linters as
"reference
let g:ale_fixers = {
      \    'python': ['yapf'],
      \}
nmap <F10> :ALEFix<CR>
"let g:ale_fix_on_save = 1

""configuration that shows the total number of warnings and errors in the status 
""line.
function! LinterStatus() abort
  let l:counts = ale#statusline#Count(bufnr(''))

  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors

  return l:counts.total == 0 ? '✨ all good ✨' : printf(
        \   '😞 %dW %dE',
        \   all_non_errors,
        \   all_errors
        \)
endfunction

set statusline=
set statusline+=%m
set statusline+=\ %f
set statusline+=%=
set statusline+=\ %{LinterStatus()}

"Semshi highlights for dark backgrounds
hi semshiLocal           ctermfg=209 guifg=#ff875f
hi semshiGlobal          ctermfg=214 guifg=#ffaf00
hi semshiImported        ctermfg=214 guifg=#ffaf00 cterm=bold gui=bold
hi semshiParameter       ctermfg=75  guifg=#5fafff
hi semshiParameterUnused ctermfg=117 guifg=#87d7ff cterm=underline gui=underline
hi semshiFree            ctermfg=218 guifg=#ffafd7
hi semshiBuiltin         ctermfg=207 guifg=#ff5fff
hi semshiAttribute       ctermfg=49  guifg=#00ffaf
hi semshiSelf            ctermfg=249 guifg=#b2b2b2
hi semshiUnresolved      ctermfg=226 guifg=#ffff00 cterm=underline gui=underline
hi semshiSelected        ctermfg=231 guifg=#ffffff ctermbg=161 guibg=#d7005f

hi semshiErrorSign       ctermfg=231 guifg=#ffffff ctermbg=160 guibg=#d70000
hi semshiErrorChar       ctermfg=231 guifg=#ffffff ctermbg=160 guibg=#d70000
sign define semshiError text=E> texthl=semshiErrorSign

"key map for search some string in file
"and in directories
map <C-F> /
map <CS-F> :vimgrep /
